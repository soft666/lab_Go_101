package main

import (
	"fmt"
)

func main() {
	fmt.Println("GO" + "LANG")
	fmt.Println("1+1 =", 1+1)
	fmt.Println(true && false)
	fmt.Println(true || false)
	fmt.Println(!true)
	fmt.Println(!false)
}
