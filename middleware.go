package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

func main() {
	h := logger(logger1(http.HandlerFunc(indexhandler)))
	err := http.ListenAndServe(":8000", h)
	log.Println(err)
}

func logger(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("req", r.Body)
		// fmt.Println("res", w)
		t := time.Now()
		h.ServeHTTP(w, r)
		diff := time.Now().Sub(t)
		log.Printf("Logger = path: %s, time: %dms", r.URL.Path, diff/time.Microsecond)
	})
}

func logger1(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("req", r.Body)
		// fmt.Println("res", w)
		t := time.Now()
		h.ServeHTTP(w, r)
		diff := time.Now().Sub(t)
		log.Printf("Logger1 = path: %s, time: %dms", r.URL.Path, diff/time.Microsecond)
	})
}

func indexhandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println(r.URL.Path)
	w.Write([]byte("Hello Gopherss."))
}
